﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollyController : MonoBehaviour
{
    public float speed = 10;
    public new  Rigidbody rigidbody; 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.W))
        {
            rigidbody.velocity = Vector3.forward * speed;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rigidbody.velocity = Vector3.back * speed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            rigidbody.velocity = Vector3.left * speed;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rigidbody.velocity = Vector3.right * speed;
        }
    }
}
