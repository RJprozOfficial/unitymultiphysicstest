﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhysicsSceneInvoker : MonoBehaviour
{
    public int count = 5;
    private PhysicsScene[] physicsScenes;
    void Start()
    {
        physicsScenes = new PhysicsScene[count];
        for (int i=0;i<count;i++)
        {
            
            LoadSceneParameters param = new LoadSceneParameters(LoadSceneMode.Additive, LocalPhysicsMode.Physics3D);
            Scene scene = SceneManager.LoadScene("PhysicsSim",param);
          
            physicsScenes[i] = scene.GetPhysicsScene();
            Debug.Log("Count :" + i);
        }

        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for(int i=0;i<physicsScenes.Length;i++)
        {
            physicsScenes[i].Simulate(Time.deltaTime);
        }
    }
}
