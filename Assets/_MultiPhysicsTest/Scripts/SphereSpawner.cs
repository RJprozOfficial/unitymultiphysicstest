﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.color = new Color(Random.value, Random.value, Random.value, 1);
        transform.position = new Vector3(Random.Range(-50, 50), transform.position.y + Random.Range(0,10f), Random.Range(-50, 50));


        Debug.Log("SphereSpawners : " +GameObject.FindObjectsOfType<SphereSpawner>().Length);
    }

    
}
